package com.epum.example.Fibonachi;

/**
 * class for working with Fibonacci numbers.
 */
public class Fibonacci {
    /**
     * 100%.
     */
    private static final int ONE_HUNDRED_PERCENT = 100;

    /**
     * variable to save the size of the array.
     */
    private final int size;

    /**
     * array to save Fibonacci numbers.
     */
    private int[] masOfFibbonaci;

    /**
     * Constructor.
     * @param size size of masOfFibbonacci
     */
    public Fibonacci(int size) {
        this.size = size;
    }

    /**
     *prints an array of Fibonacci numbers.
     */
    public void printMasOfFibonacci() {
        for (int i = 0; i < masOfFibbonaci.length; i++) {
            System.out.print(masOfFibbonaci[i] + " ");
        }

        System.out.println();
    }

    /**
     *prints the biggest odd number.
     */
    public void printTheBiggestOddNumber() {
        int theBiggestOddNumber = 1;

        for (int i = masOfFibbonaci.length - 1; i > 0; i--) {
            if (masOfFibbonaci[i] % 2 == 0) {
                theBiggestOddNumber = masOfFibbonaci[i];

                break;
            }
        }

        System.out.println("The biggest odd Number is " + theBiggestOddNumber);
    }

    /**
     *prints the biggest even number.
     */
    public void printTheBiggestEvenNumber() {
        int theBiggestEvenNumber = 1;

        for (int i = masOfFibbonaci.length - 1; i > 0; i--) {
            if (masOfFibbonaci[i] % 2 != 0) {
                theBiggestEvenNumber = masOfFibbonaci[i];

                break;
            }
        }

        System.out.println("The biggest odd Number is " + theBiggestEvenNumber);
    }

    /**
     *prints percentage of even number.
     */
    public void printPercentageOfEvenFibonacciNumbers() {
        int countEven = 0;

        for (int i = 0; i < masOfFibbonaci.length; i++) {
            if (masOfFibbonaci[i] % 2 == 0) {
                countEven++;
            }
        }

        double percentageOfEvenNumber = (double) countEven
                * ONE_HUNDRED_PERCENT / masOfFibbonaci.length;

        System.out.println("Percentage of even numbers is "
                + percentageOfEvenNumber + "%");

    }

    /**
     *prints percentage of odd number.
     */
    public void printPercentageOfOddFibonacciNumbers() {
        int countOdd = 0;

        for (int i = 0; i < masOfFibbonaci.length; i++) {
            if (masOfFibbonaci[i] % 2 != 0) {
                countOdd++;
            }
        }

        double percentageOfOddNumber = (double) countOdd
                * ONE_HUNDRED_PERCENT / masOfFibbonaci.length;

        System.out.println("Percentage of odd numbers is "
                + percentageOfOddNumber + "%");

    }

    /**
     *The function fills in an array of specified size with Fibonacci numbers.
     */
    public void setMasOfFibonacci() {
        masOfFibbonaci = new int[size];

        int previousNumber = 1;
        int previousPreviousNumber = 1;

        masOfFibbonaci[0] = previousNumber;
        masOfFibbonaci[1] = previousPreviousNumber;

        for (int i = 2; i < size; i++) {
            masOfFibbonaci[i] = previousNumber + previousPreviousNumber;

            if (i % 2 == 0) {
                previousPreviousNumber = masOfFibbonaci[i];
            } else {
                previousNumber = masOfFibbonaci[i];
            }
        }
    }
}
