package com.epum.example;

import com.epum.example.Fibonachi.Fibonacci;
import com.epum.example.Number.Number;

import java.util.Scanner;

/**
 * The main class.
 */
public class Main {
    /**
     * Here start point of the program.
     * @param args comand line values
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        byte minNumber;
        byte maxNumber;

        Number number;

        do {
            System.out.println("Enter a minimum number");

            minNumber = scanner.nextByte();

            System.out.println("Enter a maximum number");

            maxNumber = scanner.nextByte();

            number = new Number(minNumber, maxNumber);

             if (minNumber > maxNumber) {
                System.out.println("Numbers entered incorrectly");
             }

        } while (minNumber > maxNumber);

        number.printOddFromTheBeginning();

        number.printEvenFromTheEnd();

        number.printSumEvenNumber();

        number.printSumOddNumber();

        System.out.println("Enter the size of the first Fibonacci numbers");

        int sizeOfFibonacciNumber = scanner.nextInt();

        Fibonacci fibonacci = new Fibonacci(sizeOfFibonacciNumber);

        fibonacci.setMasOfFibonacci();

        fibonacci.printMasOfFibonacci();

        fibonacci.printTheBiggestEvenNumber();

        fibonacci.printTheBiggestOddNumber();

        fibonacci.printPercentageOfEvenFibonacciNumbers();

        fibonacci.printPercentageOfOddFibonacciNumbers();
    }
}
