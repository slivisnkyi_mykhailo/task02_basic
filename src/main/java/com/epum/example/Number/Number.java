package com.epum.example.Number;

/**
 * class for finding the interval of odd
 * and even numbers and actions over them.
 */
public class Number {
    /**
     *  minimum number.
     */
    private byte minNumber;

    /**
     * maximum number.
     */
    private byte maxNumber;

    /**
     * constructor.
     * @param minNumber minimum number
     * @param maxNumber maximum number
     */
    public Number(byte minNumber, byte maxNumber) {
        this.minNumber = minNumber;
        this.maxNumber = maxNumber;
    }

    /**
     *prints an array of odd numbers in the specified interval from
     * beginning to end.
     */
    public void printOddFromTheBeginning() {
        for (int i = minNumber; i <= maxNumber; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }

        System.out.println();
    }

    /**
     *prints an array of even numbers in the specified interval from
     * beginning to end.
     */
    public void printEvenFromTheEnd() {
        for (int i = maxNumber; i >= minNumber; i--) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }

        System.out.println();
    }

    /**
     *prints the sum of odd numbers at a given interval.
     * @return sum of odd numbers.
     */
    public int printSumOddNumber() {
        int result = 0;

        for (int i = minNumber; i <= maxNumber; i++) {
            if (i % 2 != 0) {
                result += i;
            }
        }

        return result;
    }

    /**
     *prints the sum of even numbers at a given interval.
     * @return sum of even numbers.
     */
    public int printSumEvenNumber() {
        int result = 0;

        for (int i = minNumber; i <= maxNumber; i++) {
            if (i % 2 == 0) {
                result += i;
            }
        }

        return result;
    }

    /**
     * getter MinNumber.
     * @return minimum number
     */
    public byte getMinNumber() {
        return minNumber;
    }

    /**
     * setter minNumber.
     * @param minNumber set minimum number
     */
    public void setMinNumber(byte minNumber) {
        this.minNumber = minNumber;
    }

    /**
     * getter maximum number.
     * @return maximum number
     */
    public byte getMaxNumber() {
        return maxNumber;
    }

    /**
     * setter maximum number.
     * @param maxNumber set maximum number
     */
    public void setMaxNumber(byte maxNumber) {
        this.maxNumber = maxNumber;
    }
}
